﻿using System.Data.Common;
using System.Data.Entity;
using LMF.Contracts.Security.Models;

namespace LMF.DataAccess
{
    public class ModelsContext : DbContext
    {
        public ModelsContext()
            : base("SqlConnectionString")
        {

        }

        public ModelsContext(DbConnection dbConnection, bool contextOwnsConnection = false)
            : base(dbConnection, contextOwnsConnection)
        {
            
        }

        public DbSet<User> Users { get; set; }
        public DbSet<UserPassword> UserPasswords { get; set; }
    }
}
