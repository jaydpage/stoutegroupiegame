using System.Data.Entity.Migrations;

namespace LMF.DataAccess.Migrations
{
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserPasswords",
                c => new
                    {
                        UserId = c.Long(nullable: false),
                        HashedPassword = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        EmailAddress = c.String(nullable: false, maxLength: 100),
                        Admin = c.Boolean(nullable: false),
                        ScanStops = c.Boolean(nullable: false),
                        SelfRegister = c.Boolean(nullable: false),
                        ApproveRegistrations = c.Boolean(nullable: false),
                        ViewReports = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.EmailAddress, unique: true);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Users", new[] { "EmailAddress" });
            DropTable("dbo.Users");
            DropTable("dbo.UserPasswords");
        }
    }
}
