using System.Collections.Concurrent;
using System.Collections.Generic;
using LMF.Game.Models;
using LMF.Game.Models.Builders;

namespace LMF.Game
{
    //todo it feels like Board needs a container, something like a world, but for now we'll just use the board as is because we aren't supporting multiple boards yet
    public class Board
    {
        private readonly ConcurrentDictionary<string, byte> _characters = new ConcurrentDictionary<string, byte>();

        public Board()
        {
            Definition = new UniformHexagonBoardDefinitionBuilder()
                .WithSizeX(100)
                .WithSizeY(100)
                .Build();
            Name = "Board";
        }

        public bool AddCharacter(string characterName)
        {
            if (_characters.ContainsKey(characterName)) return false;
            _characters.AddOrUpdate(characterName, 0, (s, b) => 0);
            return true;
        }

        public bool RemoveCharacter(string characterName)
        {
            byte value;
            return _characters.TryRemove(characterName, out value);
        }

        public UniformHexagonBoardDefinition Definition { get; set; }

        public string Name { get; set; }

        public IEnumerable<string> Characters
        {
            get { return _characters.Keys; }
        }
    }
}