﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMF.Game.Actions;
using LMF.Game.IO;

namespace LMF.Game
{
    public class InputFactory
    {
        private readonly Board _board;
        private readonly IBattleOutput _battleOutput;

        public InputFactory(Board board, IBattleOutput battleOutput)
        {
            _board = board;
            _battleOutput = battleOutput;
        }

        public ILeaveBoardInput CreateLeaveBoardInput()
        {
            return new LeaveBoardAction(_board);
        }

        public IJoinBoardInput CreateJoinBoardInput()
        {
            return new JoinBoardAction(_board);
        }

        public IStartBattleInput CreateBattleInput()
        {
            return new StartBattleAction(_board, _battleOutput);
        }
    }
}
