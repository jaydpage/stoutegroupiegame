using LMF.Game.Models;

namespace LMF.Game.IO
{
    public interface IStartBattleInput
    {
        void Start(BattleRequest battleRequest);
    }
}