﻿using LMF.Game.Models;

namespace LMF.Game.IO
{
    public interface IBattleOutput
    {
        void FinishBattle(BattleRequest battleRequest);
        void StopBattle(BattleRequest battleRequest);
    }
}