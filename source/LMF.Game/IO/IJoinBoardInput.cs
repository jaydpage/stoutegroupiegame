﻿using LMF.Game.Models;

namespace LMF.Game.IO
{
    public interface IJoinBoardInput
    {
        JoinBoardResponse Join(JoinBoardRequest joinBoardRequest);
    }
}