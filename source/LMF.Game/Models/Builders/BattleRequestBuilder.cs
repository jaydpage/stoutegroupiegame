﻿using System.Collections.Generic;

namespace LMF.Game.Models.Builders
{
    public class BattleRequestBuilder
    {
        private string _positionKey;
        private string _characterName;
        private List<string> _errors;

        public BattleRequestBuilder WithCharacterName(string characterName)
        {
            _characterName = characterName;
            return this;
        }

        public BattleRequestBuilder WithPositionKey(string positionKey)
        {
            _positionKey = positionKey;
            return this;
        }

        public BattleRequestBuilder WithErrors(params string[] errors)
        {
            _errors = _errors ?? new List<string>();
            _errors.AddRange(errors);
            return this;
        }

        public BattleRequest Build()
        {
            return new BattleRequest()
            {
                positionKey = _positionKey,
                characterName = _characterName,
                errors = _errors
            };
        }
    }
}
