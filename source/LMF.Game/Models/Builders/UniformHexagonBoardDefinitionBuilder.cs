namespace LMF.Game.Models.Builders
{
    public class UniformHexagonBoardDefinitionBuilder
    {
        private int _sizeX;
        private int _sizeY;

        public UniformHexagonBoardDefinitionBuilder WithSizeX(int sizeX)
        {
            _sizeX = sizeX;
            return this;
        }

        public UniformHexagonBoardDefinitionBuilder WithSizeY(int sizeY)
        {
            _sizeY = sizeY;
            return this;
        }

        public UniformHexagonBoardDefinition Build()
        {
            return new UniformHexagonBoardDefinition(_sizeX, _sizeY);
        }
    }
}