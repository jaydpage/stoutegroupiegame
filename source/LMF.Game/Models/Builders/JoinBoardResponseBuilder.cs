﻿using System.Collections.Generic;

namespace LMF.Game.Models.Builders
{
    public class JoinBoardResponseBuilder
    {
        private UniformHexagonBoardDefinition _boardDefinition;
        private List<string> _errors;

        public JoinBoardResponseBuilder WithBoardDefinition(UniformHexagonBoardDefinition boardDefinition)
        {
            _boardDefinition = boardDefinition;
            return this;
        }

        public JoinBoardResponseBuilder WithErrors(params string[] errors)
        {
            _errors = _errors ?? new List<string>();
            _errors.AddRange(errors);
            return this;
        }

        public JoinBoardResponse Build()
        {
            return new JoinBoardResponse()
            {
                boardDefinition = _boardDefinition,
                errors = _errors,
            };
        }
    }
}