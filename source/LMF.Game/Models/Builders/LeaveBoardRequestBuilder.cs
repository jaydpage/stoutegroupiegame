namespace LMF.Game.Models.Builders
{
    public class LeaveBoardRequestBuilder
    {
        private string _characterName;

        public LeaveBoardRequestBuilder WithCharacterName(string characterName)
        {
            _characterName = characterName;
            return this;
        }

        public LeaveBoardRequest Build()
        {
            return new LeaveBoardRequest()
            {
                characterName = _characterName
            };
        }
    }
}