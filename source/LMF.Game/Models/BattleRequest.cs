﻿using System.Collections.Generic;

namespace LMF.Game.Models
{
    public class BattleRequest
    {
        //todo it feels wierd to have errors on a request, maybe this should be split into a request and response, or maybe a battle shouldn't be modeled using request/response but instead as state that is passed between client and server as the battle progresses
        //todo model a proper error object
        public List<string> errors;
        public string positionKey { get; set; }
        public string characterName { get; set; }
    }
}