﻿namespace LMF.Game.Models
{
    public class UniformHexagonBoardDefinition
    {
        public int sizeX { get; set; }
        public int sizeY { get; set; }

        public UniformHexagonBoardDefinition(int sizeX, int sizeY)
        {
            this.sizeX = sizeX;
            this.sizeY = sizeY;
        }
    }
}