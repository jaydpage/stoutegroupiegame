﻿using System.Collections.Generic;

namespace LMF.Game.Models
{
    public class JoinBoardResponse
    {
        public bool hasErrors
        {
            get { return errors != null && errors.Count > 0; }
        }
        //todo model a proper error object
        public List<string> errors { get; set; }
        public UniformHexagonBoardDefinition boardDefinition { get; set; }
    }
}