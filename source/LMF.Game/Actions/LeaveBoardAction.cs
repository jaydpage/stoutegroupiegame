﻿using LMF.Game.IO;
using LMF.Game.Models;

namespace LMF.Game.Actions
{
    public class LeaveBoardAction : ILeaveBoardInput
    {
        private readonly Board _board;

        public LeaveBoardAction(Board board)
        {
            _board = board;
        }

        public void Leave(LeaveBoardRequest leaveBoardRequest)
        {
            _board.RemoveCharacter(leaveBoardRequest.characterName);
        }
    }
}