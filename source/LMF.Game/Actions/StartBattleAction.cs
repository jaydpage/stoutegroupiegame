using System.Collections.Generic;
using System.Linq;
using LMF.Game.IO;
using LMF.Game.Models;

namespace LMF.Game.Actions
{
    public class StartBattleAction : IStartBattleInput
    {
        private readonly Board _board;
        private readonly IBattleOutput _battleOutput;

        public StartBattleAction(Board board, IBattleOutput battleOutput)
        {
            _board = board;
            _battleOutput = battleOutput;
        }

        public void Start(BattleRequest battleRequest)
        {
            if (IsCharacterOnBoard(battleRequest.characterName))
            {
                AddCharacterNotOnBoardError(battleRequest);
                _battleOutput.StopBattle(battleRequest);
                return;
            }

            _battleOutput.FinishBattle(battleRequest);
        }

        private bool IsCharacterOnBoard(string characterName)
        {
            return !_board.Characters.Contains(characterName);
        }

        private void AddCharacterNotOnBoardError(BattleRequest battleRequest)
        {
            battleRequest.errors = battleRequest.errors ?? new List<string>();
            battleRequest.errors.Add($"Character '{battleRequest.characterName}' isn't on the board.");
        }
    }
}