﻿using LMF.Game.IO;
using LMF.Game.Models;
using LMF.Game.Models.Builders;

namespace LMF.Game.Actions
{
    public class JoinBoardAction : IJoinBoardInput
    {
        private readonly Board _board;

        public JoinBoardAction(Board board)
        {
            _board = board;
        }

        public JoinBoardResponse Join(JoinBoardRequest joinBoardRequest)
        {
            if (_board.AddCharacter(joinBoardRequest.characterName))
            {
                return new JoinBoardResponseBuilder()
                    .WithBoardDefinition(_board.Definition)
                    .Build();
            }

            return new JoinBoardResponseBuilder()
                .WithErrors($"The character '{joinBoardRequest.characterName}' already exists on the board.")
                .Build();
        }
    }
}