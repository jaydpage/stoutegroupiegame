﻿using LMF.Game.Actions;
using LMF.Game.IO;
using LMF.Game.Models;
using LMF.Game.Models.Builders;
using NUnit.Framework;

namespace LMF.Game.Tests.Actions
{
    [TestFixture]
    public class LeaveBoardActionTests
    {
        [Test]
        public void Leave_GivenCharacterIsOnBoard_ShouldRemoveCharacterFromBoard()
        {
            //Arrange
            var characterName = "Mrs. Lance";
            var leavingCharacterName = "Tubbeh";
            var expectedCharacters = new[] { characterName };
            var leaveBoardRequest = CreateLeaveBoardRequest(leavingCharacterName);
            var board = CreateBoard();
            board.AddCharacter(characterName);
            board.AddCharacter(leavingCharacterName);
            var leaveBoardAction = CreateLeaveBoardAction(board);
            //Act
            leaveBoardAction.Leave(leaveBoardRequest);
            //Assert
            CollectionAssert.AreEquivalent(expectedCharacters, board.Characters);
        }

        [Test]
        public void Leave_GivenCharacterIsNotOnBoard_ShouldLeaveCharactersUnchanged()
        {
            //Arrange
            var characterName = "Mrs. Lance";
            var leavingCharacterName = "Bob";
            var expectedCharacters = new[] { characterName };
            var leaveBoardRequest = CreateLeaveBoardRequest(leavingCharacterName);
            var board = CreateBoard();
            board.AddCharacter(characterName);
            var leaveBoardAction = CreateLeaveBoardAction(board);
            //Act
            leaveBoardAction.Leave(leaveBoardRequest);
            //Assert
            CollectionAssert.AreEquivalent(expectedCharacters, board.Characters);
        }

        //todo consider making a builder or a factory for this
        private Board CreateBoard()
        {
            return new Board();
        }

        private LeaveBoardRequest CreateLeaveBoardRequest(string characterName)
        {
            return new LeaveBoardRequestBuilder()
                .WithCharacterName(characterName)
                .Build();
        }

        //todo move this to an input factory
        private ILeaveBoardInput CreateLeaveBoardAction(Board board)
        {
            return new LeaveBoardAction(board);
        }
    }
}
