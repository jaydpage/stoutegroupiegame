﻿using LMF.Game.Actions;
using LMF.Game.IO;
using LMF.Game.Models;
using LMF.Game.Models.Builders;
using NSubstitute;
using NUnit.Framework;

namespace LMF.Game.Tests.Actions
{
    [TestFixture]
    public class StartBattleActionTests
    {
        [Test]
        public void Start_GivenCharacterIsOnBoard_ShouldSendFinishBattle()
        {
            //Arrange
            var positionKey = "tile-1-2";
            var characterName = "Bob";
            var board = CreateBoardWithCharacter(characterName);

            var battleOutput = Substitute.For<IBattleOutput>();
            BattleRequest actualBattleRequest = null;
            battleOutput.FinishBattle(Arg.Do<BattleRequest>(request => actualBattleRequest = request));

            var battleRequest = new BattleRequestBuilder()
                .WithCharacterName(characterName)
                .WithPositionKey(positionKey)
                .Build();
            var startBattleAction = CreateStartBattleAction(board, battleOutput);
            //Act
            startBattleAction.Start(battleRequest);
            //Assert
            Assert.NotNull(actualBattleRequest);
            Assert.AreEqual(positionKey, actualBattleRequest.positionKey);
            Assert.AreEqual(characterName, actualBattleRequest.characterName);
            Assert.Null(actualBattleRequest.errors);
        }

        [Test]
        public void Start_GivenCharacterIsOnBoard_ShouldNotSendStopBattle()
        {
            //Arrange
            var positionKey = "tile-15-62";
            var characterName = "Ver";
            var board = CreateBoardWithCharacter(characterName);

            var battleOutput = Substitute.For<IBattleOutput>();

            var battleRequest = new BattleRequestBuilder()
                .WithCharacterName(characterName)
                .WithPositionKey(positionKey)
                .Build();
            var startBattleAction = CreateStartBattleAction(board, battleOutput);
            //Act
            startBattleAction.Start(battleRequest);
            //Assert
            battleOutput.DidNotReceive().StopBattle(Arg.Any<BattleRequest>());
        }

        [Test]
        public void Start_GivenCharacterIsNotOnBoard_ShouldSendStopBattle()
        {
            //Arrange
            var positionKey = "tile-65-34";
            var characterName = "Tubbeh";
            var board = CreateBoard();

            var battleOutput = Substitute.For<IBattleOutput>();
            BattleRequest actualBattleRequest = null;
            battleOutput.StopBattle(Arg.Do<BattleRequest>(request => actualBattleRequest = request));

            var battleRequest = new BattleRequestBuilder()
                .WithCharacterName(characterName)
                .WithPositionKey(positionKey)
                .Build();
            var startBattleAction = CreateStartBattleAction(board, battleOutput);
            //Act
            startBattleAction.Start(battleRequest);
            //Assert
            Assert.NotNull(actualBattleRequest);
            Assert.NotNull(actualBattleRequest.errors);
            Assert.AreEqual(1, actualBattleRequest.errors.Count);
            StringAssert.Contains(characterName, actualBattleRequest.errors[0]);
            StringAssert.Contains("isn't on the board", actualBattleRequest.errors[0]);
        }

        [Test]
        public void Start_GivenCharacterIsNotOnBoard_ShouldNotSendFinishBattle()
        {
            //Arrange
            var positionKey = "tile-65-34";
            var characterName = "Tubbeh";
            var board = CreateBoard();

            var battleOutput = Substitute.For<IBattleOutput>();
            
            var battleRequest = new BattleRequestBuilder()
                .WithCharacterName(characterName)
                .WithPositionKey(positionKey)
                .Build();
            var startBattleAction = CreateStartBattleAction(board, battleOutput);
            //Act
            startBattleAction.Start(battleRequest);
            //Assert
            battleOutput.DidNotReceive().FinishBattle(Arg.Any<BattleRequest>());
        }

        private Board CreateBoardWithCharacter(string characterName)
        {
            var board = CreateBoard();

            var joinBoardRequest = new JoinBoardRequestBuilder()
                .WithCharacterName(characterName)
                .Build();
            var boardAction = CreateJoinBoardAction(board);
            boardAction.Join(joinBoardRequest);

            return board;
        }

        //todo consider making a builder or a factory for this
        private Board CreateBoard()
        {
            return new Board();
        }

        //todo move this to an input factory
        private IStartBattleInput CreateStartBattleAction(Board board, IBattleOutput battleOutput)
        {
            battleOutput = battleOutput ?? Substitute.For<IBattleOutput>();
            return new StartBattleAction(board, battleOutput);
        }

        //todo move this to an input factory
        private IJoinBoardInput CreateJoinBoardAction(Board board)
        {
            return new JoinBoardAction(board);
        }
    }
}