using System.Collections.Generic;
using LMF.Contracts.Security.Models;

namespace LMF.Contracts.Security
{
    public interface IUserRoleRepository
    {
        IEnumerable<UserRole> Find(string emailAddress);
    }
}