using LMF.Contracts.Security.Models;

namespace LMF.Contracts.Security
{
    public interface IUserRepository
    {
        User Find(string emailAddress);
        long CountUsers();
    }
}