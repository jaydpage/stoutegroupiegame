namespace LMF.Contracts.Security.Models
{
    public class UserRole
    {
        public string Name { get; set; }
    }
}