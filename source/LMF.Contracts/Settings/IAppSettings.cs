﻿namespace LMF.Contracts.Settings
{
    public interface IAppSettings
    {
        string SqlConnectionString { get; }
    }
}
