﻿(function() {
    'use strict';

    var app = angular.module('app');

    app.directive('entityErrors', [function () {
        function link (scope, element, attrs) {
            function populateErrors(validationErrors) {
                scope.entityErrors = {};
                var i = 0;
                _(validationErrors).each(function (validationError) {
                    if (validationError.propertyName === scope.propertyName) {
                        i++;
                        scope.entityErrors["error" + i] = validationError.errorMessage;
                    }
                });
            }

            scope.$watch(scope.propertyName, function () {
                if (!scope.entity) return;
                populateErrors(scope.entity.entityAspect.getValidationErrors());
            });

            scope.$watch(scope.entity, function () {
                scope.entity.entityAspect.validationErrorsChanged.subscribe(function () {
                    populateErrors(scope.entity.entityAspect.getValidationErrors());
                });
            });
        }

        var template = '<div data-ng-messages="entityErrors">';
        template += '   <div data-ng-repeat="(key, value) in entityErrors">';
        template += '       <div data-ng-message-exp="key" class="errorMessage">{{value}}</div>';
        template += '   </div>';
        template += '</div>';

        return {
            link: link,
            restrict: 'E',
            scope: {
                entity: '=',
                propertyName: '@'
            },
            template: template,
            replace: true
        };
    }]);
}());