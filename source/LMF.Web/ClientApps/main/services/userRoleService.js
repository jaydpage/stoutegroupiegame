﻿(function () {
    'use strict';

    var serviceId = 'userRoleService';

    function userRoleService(common, config, $http, dataService, _) {

        function loadUserRoles() {
            function extractResults(data) {
                return _(data.results).map(function(role) {
                    return role.Name;
                });
            }

            var query = dataService.query.from("UserRoles");
            var promise = dataService.executeQuery(query)
                .then(extractResults);

            return promise;
        }

        function getUserRoles() {
            return loadUserRoles();
        }

        var service = {
            getUserRoles: getUserRoles
        };

        return service;
    }

    angular.module('app').factory(serviceId, ['common', 'config', '$http', 'dataService', '_', userRoleService]);
}());