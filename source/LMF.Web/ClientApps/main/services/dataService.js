﻿(function () {
    'use strict';

    var serviceId = 'dataService';

    function dataService(common, config, breeze, localforage) {
        var entityManager = new breeze.EntityManager(config.modelsServiceName);

        function executeQuery(query) {
            function queryFailed(error) {
                common.logger.logError(error.message);
                return common.$q.reject(error); // so downstream promise users know it failed
            }

            return entityManager.executeQuery(query)
                .catch(queryFailed);
        }

        function loadNavigationProperty(entity, property) {
            function queryFailed(error) {
                common.logger.logError(error.message);
                return common.$q.reject(error); // so downstream promise users know it failed
            }

            return entity.entityAspect.loadNavigationProperty(property)
                .catch(queryFailed);
        }

        function waitForQueries(queries, context) {
            function safeSetIsExecutingQueryOnContext(value) {
                if (context) {
                    if (value) {
                        context.isExecutingQuery = (context.isExecutingQuery) ? context.isExecutingQuery + 1 : 1;
                    } else {
                        context.isExecutingQuery = (context.isExecutingQuery) ? context.isExecutingQuery - 1 : 0;
                    }
                }
            }

            function queriesSuccessful(data) {
                safeSetIsExecutingQueryOnContext(false);
                return common.$q.resolve(data);
            }

            function queriesFailed(error) {
                safeSetIsExecutingQueryOnContext(false);
                return common.$q.reject(error);
            }

            safeSetIsExecutingQueryOnContext(true);
            return common.$q.all(queries)
                .then(queriesSuccessful)
                .catch(queriesFailed);
        }

        function createEntity(type, initialData) {
            return entityManager.createEntity(type, initialData);
        }

        function createDetachedEntity(type, initialData) {
            return entityManager.createEntity(type, initialData, breeze.EntityState.Detached);
        }

        function attachEntity(entity, isNew) {
            var entityState = isNew ? breeze.EntityState.Added : breeze.EntityState.Modified;
            return entityManager.attachEntity(entity, entityState);
        }

        function saveChanges() {
            function saveFailed(error) {
                if (error.entityErrors) {
                    return common.$q.reject(error); // so downstream promise users know it failed
                }

                var reason = error.message;
                reason = "Failed to save changes: " + reason;
                common.logger.logError(reason);

                return common.$q.reject(error); // so downstream promise users know it failed
            }

            return entityManager.saveChanges()
                .catch(saveFailed);
        }

        function resilientSaveChanges() {
            function saveFailed(error) {
                var reason = "Failed to save changes to local storage: " + error;
                common.logger.logError(reason);

                return common.$q.reject(error); // so downstream promise users know it failed
            }

            function saveDataToLocalStorage() {
                var allData = entityManager.exportEntities();
                return localforage.setItem('AllData', allData)
                    .catch(saveFailed);
            }

            function clearLocalStorage() {
                if (entityManager.getChanges().length) {
                    return saveDataToLocalStorage();
                }

                return localforage.removeItem('AllData')
                    .catch(saveFailed);
            }

            return saveDataToLocalStorage()
                .then(saveChanges)
                .then(clearLocalStorage);
        }

        function recoverDataFromLocalStorage() {
            function loadFailed(error) {
                var reason = "Failed to load data from local storage: " + error;
                common.logger.logError(reason);

                return common.$q.reject(error); // so downstream promise users know it failed
            }

            function loadDataFromLocalStorage() {
                return localforage.getItem('AllData')
                    .catch(loadFailed);
            }

            function loadDataIntoEntityManager(data) {
                if (!data) {
                    return common.$q.then();
                }

                entityManager.importEntities(data);
                return common.$q.then();
            }

            return loadDataFromLocalStorage()
                .then(loadDataIntoEntityManager);
        }

        function fetchMetadata() {
            function fetchFailed(error) {
                var reason = error.message;
                reason = "Failed to save changes: " + reason;
                common.logger.logError(reason);

                return common.$q.reject(error); // so downstream promise users know it failed
            }

            return entityManager.fetchMetadata()
                .catch(fetchFailed);
        }

        function registerEntityTypeCtor(type, ctor) {
            entityManager.metadataStore.registerEntityTypeCtor(type, ctor);
        }

        function serializeEntity(entity) {
            var exportedEntityJson = entityManager.exportEntities([entity], false);
            var exportedEntity = JSON.parse(exportedEntityJson);
            var rawEntity = exportedEntity.entityGroupMap[entity.entityAspect._entityKey.entityType.name].entities[0];

            return JSON.stringify(rawEntity);
        }

        function getEntities(entityTypes, entityState) {
            return entityManager.getEntities(entityTypes, entityState);
        }

        var service = {
            waitForQueries: waitForQueries,
            executeQuery: executeQuery,
            loadNavigationProperty: loadNavigationProperty,
            createEntity: createEntity,
            createDetachedEntity: createDetachedEntity,
            attachEntity: attachEntity,
            saveChanges: saveChanges,
            resilientSaveChanges: resilientSaveChanges,
            fetchMetadata: fetchMetadata,
            getEntities: getEntities,
            registerEntityTypeCtor: registerEntityTypeCtor,
            query: breeze.EntityQuery,
            predicate: breeze.Predicate,
            entityStates: breeze.EntityState,
            serializeEntity: serializeEntity,
            recoverDataFromLocalStorage: recoverDataFromLocalStorage
        };

        return service;
    }

    angular.module('app').factory(serviceId, ['common', 'config', 'breeze', 'localforage', dataService]);
}());