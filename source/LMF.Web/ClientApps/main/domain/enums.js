﻿(function () {
    'use strict';

    var serviceId = 'enums';

    function enums(_) {
        function values() {
            return _(this).map(function(value) {
                return value;
            }).filter(function(value) {
                return !_.isFunction(value);
            });
        }

        var userRoles = {
            admin: 'Admin',
            values: values
        };

        return {
            userRoles: userRoles
        };
    }

    angular.module('app').factory(serviceId, ['_', enums]);
}());