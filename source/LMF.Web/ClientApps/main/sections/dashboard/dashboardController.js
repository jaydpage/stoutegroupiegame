﻿(function () {
    'use strict';
    var controllerId = 'dashboard';

    function dashboard(common, $mdSidenav) {
        var vm = this;

        function toggleList() {
            //todo implement this so that you don't have to do it on every view and so that it closes when you select something from the menu
            $mdSidenav('left').toggle();
        }

        function activate() {
            common.activateController([], controllerId)
                .then(function() {
                    //do stuff after controller has loaded
                });
        }

        vm.toggleList = toggleList;

        activate();
    }

    angular.module('app').controller(controllerId, ['common', '$mdSidenav', dashboard]);
}());