﻿(function () {
    'use strict';
    var controllerId = 'users';

    function users(common, $scope, dataService, dialogs, $mdDialog) {
        var vm = this;

        function loadData() {
            function addUsersToScope(data) {
                $scope.users = data.results;
            }

            var query = dataService.query.from("Users");
            var promise = dataService.executeQuery(query)
                .then(addUsersToScope);

            return dataService.waitForQueries([promise], $scope);
        }

        function newUser(event) {
            var user = dataService.createEntity("User");
            var userDetails = vm.editUser(user, true, event);
            userDetails.then(vm.loadData);
        }

        function editUser(user, isNew, event) {
            return $mdDialog.show({
                controller: 'userDetails',
                controllerAs: 'vm',
                templateUrl: '/ClientApps/main/sections/users/userDetails.html',
                parent: angular.element(document.body),
                targetEvent: event,
                locals: {
                    options: {
                        user: user,
                        isNew: isNew
                    }
                }
            });
        }

        function deleteUser(user, event) {
            function deleteUserAndRefresh() {
                user.entityAspect.setDeleted();
                return dataService.saveChanges()
                    .then(vm.loadData);
            }

            dialogs.deletePrompt('Are you sure you want to delete ' + user.EmailAddress + '?', event)
                .then(deleteUserAndRefresh);
        }

        function activate() {
            var loadPromise = vm.loadData();

            common.activateController([loadPromise], controllerId)
                .then(function () {
                    //you can here do something when controller is ready
                });
        }

        vm.title = 'Users';
        vm.newUser = newUser;
        vm.editUser = editUser;
        vm.deleteUser = deleteUser;
        vm.loadData = loadData;

        activate();
    }

    angular.module('app').controller(controllerId, ['common', '$scope', 'dataService', 'dialogs', '$mdDialog', users]);
}());