﻿(function () { 
    'use strict';
    
    var controllerId = 'sidebar';
    //$mdSidenav('left').toggle();
    function sidebar($route, config, routes, userRoleService, _) {
        var vm = this;

        function getUserRoles() {
            return userRoleService.getUserRoles();
        }

        function populateNavRoutes(roles) {
            function isRouteAvailableToCurrentUser(userRoles, route) {
                return !route.config.settings.roles || _.intersection(route.config.settings.roles, userRoles).length;
            }

            function isNavRoute(route) {
                return route.config.settings && route.config.settings.nav;
            }

            vm.navRoutes = routes.filter(function (r) {
                return isNavRoute(r) && isRouteAvailableToCurrentUser(roles, r);
            }).sort(function (r1, r2) {
                return r1.config.settings.nav - r2.config.settings.nav;
            });
        }

        function isCurrent(route) {
            if (!route.config.title || !$route.current || !$route.current.title) {
                return '';
            }
            var menuName = route.config.title;
            return $route.current.title.substr(0, menuName.length) === menuName ? 'current' : '';
        }
        
        function activate() {
            getUserRoles().then(populateNavRoutes);
        }

        vm.isCurrent = isCurrent;

        activate();
    }

    angular.module('app').controller(controllerId,['$route', 'config', 'routes', 'userRoleService', '_', sidebar]);

}());
