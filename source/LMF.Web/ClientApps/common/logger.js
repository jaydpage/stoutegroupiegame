(function () {
    'use strict';
    
    function logger($log, toastr) {
        var service = {};

        function logIt(message, data, source, showToast, toastType) {
            showToast = (showToast === undefined) ? true : showToast;
            var write = (toastType === 'error') ? $log.error : $log.log;
            source = source ? '[' + source + '] ' : '';
            write(source, message, data);
            if (showToast) {
                if (toastType === 'error') {
                    toastr.error(message);
                } else if (toastType === 'warning') {
                    toastr.warning(message);
                } else if (toastType === 'success') {
                    toastr.success(message);
                } else {
                    toastr.info(message);
                }
            }
        }

        function log(message, data, source, showToast) {
            logIt(message, data, source, showToast, 'info');
        }

        function logWarning(message, data, source, showToast) {
            logIt(message, data, source, showToast, 'warning');
        }

        function logSuccess(message, data, source, showToast) {
            logIt(message, data, source, showToast, 'success');
        }

        function logError(message, data, source, showToast) {
            logIt(message, data, source, showToast, 'error');
        }

        service.log = log;
        service.logError = logError;
        service.logSuccess = logSuccess;
        service.logWarning = logWarning;

        return service;
    }

    angular.module('common').factory('logger', ['$log', 'toastr', logger]);
}());