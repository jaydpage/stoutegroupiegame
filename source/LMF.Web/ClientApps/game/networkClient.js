﻿function NetworkClient(connection) {
    var self = this;
    self.onBattleFinishedCallbacks = [];
    self.onBattleStoppedCallbacks = [];
    self.connection = connection;
    self.hub = connection.worldHub;
    self.hub.client.onBattleFinished = function (battleResponse) { self.__onBattleFinishedHandler(battleResponse) };
    self.hub.client.onBattleStopped = function (battleResponse) { self.__onBattleStoppedHandler(battleResponse);}
}

NetworkClient.prototype.__onBattleFinishedHandler = function (battleResponse) {
    var self = this;
    for (var i = 0; i < self.onBattleFinishedCallbacks.length; i++) {
        var callbackInfo = self.onBattleFinishedCallbacks[i];
        callbackInfo.callback.call(callbackInfo.thisForCallback, battleResponse);
    }
}

NetworkClient.prototype.__onBattleStoppedHandler = function (battleResponse) {
    var self = this;
    for (var i = 0; i < self.onBattleStoppedCallbacks.length; i++) {
        var callbackInfo = self.onBattleStoppedCallbacks[i];
        callbackInfo.callback.call(callbackInfo.thisForCallback, battleResponse);
    }
}

NetworkClient.prototype.connect = function () {
    var self = this;
    self.connection.hub.logging = true;
    self.connection.hub.stop();
    return self.connection.hub.start();
}

NetworkClient.prototype.joinBoard = function (joinBoardRequest) {
    var self = this;
    return self.hub.server.joinBoard(joinBoardRequest);
}

NetworkClient.prototype.startBattle = function(battleRequest) {
    var self = this;
    return self.hub.server.startBattle(battleRequest);
}

NetworkClient.prototype.addOnBattleFinishedCallback = function (callback, thisForCallback) {
    var self = this;
    if (_.isFunction(callback)) {
        var callbackInfo = new CallbackInfoBuilder()
            .withCallback(callback)
            .withThis(thisForCallback)
            .build();
        self.onBattleFinishedCallbacks.push(callbackInfo);
    }
}

NetworkClient.prototype.addOnBattleStoppedCallback = function (callback, thisForCallback) {
    var self = this;
    if (_.isFunction(callback)) {
        var callbackInfo = new CallbackInfoBuilder()
            .withCallback(callback)
            .withThis(thisForCallback)
            .build();
        self.onBattleStoppedCallbacks.push(callbackInfo);
    }
}