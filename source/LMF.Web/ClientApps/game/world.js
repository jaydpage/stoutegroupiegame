﻿function World(elementName, network) {
    var self = this;
    self.elementName = elementName;
    self.network = network;
    self.spritesInView = {};
    self.network.addOnBattleFinishedCallback(self.finishBattle, self);
    self.network.addOnBattleStoppedCallback(self.stopBattle, self);
}

World.prototype.connect = function () {
    var self = this;
    return self.network.connect();
}

World.prototype.joinBoard = function(characterName) {
    var self = this;
    var joinBoardRequest = new JoinBoardRequestBuilder()
        .withCharacterName(characterName)
        .build();
    return self.network.joinBoard(joinBoardRequest).then(function (joinBoardResponse) {
        if (joinBoardResponse.hasErrors) {
            return $.Deferred().reject(joinBoardResponse.errors);
        }

        self.board = new Board(joinBoardResponse.boardDefinition.sizeX, joinBoardResponse.boardDefinition.sizeY);
    });
}

World.prototype.startBattle = function(positionKey) {
    var self = this;
    var battleRequest = new BattleRequestBuilder()
        .withPositionKey(positionKey)
        .build();

    self.network.startBattle(battleRequest);
}

World.prototype.finishBattle = function (battleResponse) {
    var self = this;
    var position = self.board.takePosition(battleResponse);
    if (position && self.spritesInView[battleResponse.positionKey]) {
        self.__updateTileSpriteStatus(self.spritesInView[battleResponse.positionKey], position);
    }
}

World.prototype.stopBattle = function (battleResponse) {
    var self = this;
    //todo show errors to the user
    console.log(battleResponse);
}

World.prototype.__updateTileSpriteStatus = function (tileSprite, boardPosition) {
    tileSprite.tint = boardPosition.taken ? 0x611DA6 : 0xF6E91B;
}

World.prototype.render = function () {
    var self = this;
    var previousPointerPosition;
    var dragStartingPoint;

    var gameContentElement = $('#' + self.elementName);
    var gameWidth = gameContentElement.width();
    var gameHeight = gameContentElement.height();

    var game = new Phaser.Game(gameWidth, gameHeight, Phaser.AUTO, self.elementName, { preload: onPreload, create: onCreate, update: onUpdate });

    $(window).resize(resizeGame);

    function onPreload() {
        game.load.image(self.board.tileImage.name, self.board.tileImage.url);
    }

    function onCreate() {
        game.world.setBounds(0, 0, self.board.width, self.board.height);
        game.stage.backgroundColor = self.board.backgroundColour;
        game.input.addMoveCallback(onInputMove, this);
    }

    function onUpdate() {
        for (var positionKey in self.board.positions) {
            var boardPosition = self.board.positions[positionKey];
            var isPositionOnCamera = wouldPositionBeOnCamera(boardPosition);

            if (!self.spritesInView[boardPosition.key] && isPositionOnCamera) {
                var tileSprite = createTileSprite(boardPosition);
                self.spritesInView[boardPosition.key] = tileSprite;

            } else if (self.spritesInView[boardPosition.key] && !isPositionOnCamera) {
                self.spritesInView[boardPosition.key].destroy();
                self.spritesInView[boardPosition.key] = undefined;
            }
        }
    }

    function wouldPositionBeOnCamera(boardPosition) {
        return game.camera.view.contains(boardPosition.tileX, boardPosition.tileY) ||
            game.camera.view.contains(boardPosition.tileX + boardPosition.tileWidth, boardPosition.tileY + boardPosition.tileHeight) ||
            game.camera.view.contains(boardPosition.tileX + boardPosition.tileWidth, boardPosition.tileY) ||
            game.camera.view.contains(boardPosition.tileX, boardPosition.tileY + boardPosition.tileHeight);
    }

    function createTileSprite(boardPosition) {
        var tileSprite = game.add.sprite(boardPosition.tileX, boardPosition.tileY, boardPosition.tileImage);
        tileSprite.positionKey = boardPosition.key;
        tileSprite.inputEnabled = true;
        tileSprite.input.pixelPerfectClick = true;
        tileSprite.events.onInputUp.add(onTileInputUp, this);

        self.__updateTileSpriteStatus(tileSprite, boardPosition);

        return tileSprite;
    }

    function onInputMove() {
        moveCameraByPointer(game.input.mousePointer);
        moveCameraByPointer(game.input.pointer1);
    }

    function onTileInputUp(tileSprite, pointer) {
        if (dragStartingPoint && (dragStartingPoint.x !== pointer.position.x || dragStartingPoint.y !== pointer.position.y)) return;
        self.startBattle(tileSprite.positionKey);
    }

    function resizeGame() {
        var newGameWidth = gameContentElement.width();
        var newGameHeight = gameContentElement.height();

        game.width = newGameWidth;
        game.height = newGameHeight;

        if (game.renderType === Phaser.WEBGL) {
            game.renderer.resize(newGameWidth, newGameHeight);
        }
    }

    function moveCameraByPointer(pointer) {
        if (!pointer.timeDown) {
            return;
        }
        if (pointer.isDown) {
            if (previousPointerPosition) {
                game.camera.x += previousPointerPosition.x - pointer.position.x;
                game.camera.y += previousPointerPosition.y - pointer.position.y;
            } else {
                dragStartingPoint = pointer.position.clone();
            }
            previousPointerPosition = pointer.position.clone();
        }
        if (pointer.isUp) {
            previousPointerPosition = null;
            dragStartingPoint = null;
        }
    }
}