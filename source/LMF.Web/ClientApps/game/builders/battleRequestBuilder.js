﻿function BattleRequestBuilder() {

}

BattleRequestBuilder.prototype.withPositionKey = function (positionKey) {
    var self = this;
    self.positionKey = positionKey;
    return self;
}

BattleRequestBuilder.prototype.build = function () {
    var self = this;

    return {
        positionKey: self.positionKey
    }
}