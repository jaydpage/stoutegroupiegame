﻿function Board(sizeX, sizeY) {
    var self = this;

    self.tileImage = {
        name: 'hexagon',
        url: '/ClientApps/main/sections/game/images/hexagon.png',
        height: 70,
        width: 80
    }

    self.boardSizeX = sizeX;
    self.boardSizeY = sizeY;
    self.backgroundColour = '#ffffff';
    self.positions = self.__getBoardPositions();

    self.boardWidth = (self.boardSizeX / 2) + self.tileImage.width;
    self.boardHeight = self.boardSizeY + self.tileImage.height;
}

Board.prototype.takePosition = function (battleResponse) {
    var self = this;
    var position = self.positions[battleResponse.positionKey];
    position.taken = true;

    return position;
}

Board.prototype.__getBoardPositions = function () {
    var self = this;
    var positions = {};
    for (var boardPositionX = 0; boardPositionX < self.boardSizeX / 2; boardPositionX++) {
        for (var boardPositionY = 0; boardPositionY < self.boardSizeY; boardPositionY++) {
            var boardPosition = {
                key: "tile-" + boardPositionX + "-" + boardPositionY,
                x: boardPositionX,
                y: boardPositionY,
                tileImage: "hexagon",
                tileHeight: 70,
                tileWidth: 80
            };

            boardPosition.tileX = self.__calculateTilePostionX(boardPosition);
            boardPosition.tileY = self.__calculateTilePositionY(boardPosition);

            positions[boardPosition.key] = boardPosition;
        }
    }

    return positions;
}

Board.prototype.__calculateTilePostionX = function (boardPosition) {
    var sectorWidth = boardPosition.tileWidth / 4 * 3;
    return boardPosition.tileWidth * boardPosition.x * 1.5 + sectorWidth * (boardPosition.y % 2);
}

Board.prototype.__calculateTilePositionY = function (boardPosition) {
    return boardPosition.tileHeight * boardPosition.y / 2;
}