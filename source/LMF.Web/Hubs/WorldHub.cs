﻿using System.Collections.Concurrent;
using System.Threading.Tasks;
using LMF.Game;
using LMF.Game.Actions;
using LMF.Game.IO;
using LMF.Game.Models;
using LMF.Game.Models.Builders;
using Microsoft.AspNet.SignalR;

namespace LMF.Web.Hubs
{
    [Authorize]
    public class WorldHub : Hub, IBattleOutput
    {
        //todo move these to some sort of bootstrapping
        private static readonly Board Board = new Board();
        private readonly InputFactory _inputFactory;
        //todo consider putting this behind a state class, maybe a repository or gateway
        private static readonly ConcurrentDictionary<string, string> ConnectionCharacters = new ConcurrentDictionary<string, string>();

        public WorldHub()
        {
            _inputFactory = new InputFactory(Board, this);
        }

        public void StartBattle(BattleRequest battleRequest)
        {
            AddCharacterNameToBattle(battleRequest);
            var battleInput = _inputFactory.CreateBattleInput();
            battleInput.Start(battleRequest);
        }

        private void AddCharacterNameToBattle(BattleRequest battleRequest)
        {
            string characterName;
            ConnectionCharacters.TryGetValue(Context.ConnectionId, out characterName);
            battleRequest.characterName = characterName;
        }

        public JoinBoardResponse JoinBoard(JoinBoardRequest joinBoardRequest)
        {
            var joinBoardInput = _inputFactory.CreateJoinBoardInput();
            var joinBoardResponse = joinBoardInput.Join(joinBoardRequest);
            if (joinBoardResponse.hasErrors) return joinBoardResponse;

            Groups.Add(Context.ConnectionId, Board.Name).Wait();
            ConnectionCharacters.AddOrUpdate(Context.ConnectionId, joinBoardRequest.characterName, (s, s1) => joinBoardRequest.characterName);

            return joinBoardResponse;
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            LeaveBoard();
            return base.OnDisconnected(stopCalled);
        }

        private void LeaveBoard()
        {
            string characterName;
            if (ConnectionCharacters.TryRemove(Context.ConnectionId, out characterName))
            {
                var leaveBoardRequest = new LeaveBoardRequestBuilder()
                    .WithCharacterName(characterName)
                    .Build();
                var leaveBoardInput = _inputFactory.CreateLeaveBoardInput();
                leaveBoardInput.Leave(leaveBoardRequest);
            }
        }

        void IBattleOutput.FinishBattle(BattleRequest battleRequest)
        {
            Clients.Group(Board.Name).onBattleFinished(battleRequest);
        }

        void IBattleOutput.StopBattle(BattleRequest battleRequest)
        {
            Clients.Group(Board.Name).onBattleStopped(battleRequest);
        }
    }
}