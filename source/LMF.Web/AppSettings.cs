﻿using System.Web.Configuration;
using LMF.Contracts.Settings;

namespace LMF.Web
{
    public class AppSettings : IAppSettings
    {
        public string SqlConnectionString
        {
            get { return WebConfigurationManager.ConnectionStrings["SqlConnectionString"].ConnectionString; }
        }

        private string GetSettingOrDefault(string fieldName, string @default)
        {
            var setting = WebConfigurationManager.AppSettings[fieldName];
            if (setting != null) return setting;
            return @default;
        }
    }
}
