﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web.Http;
using Breeze.ContextProvider;
using Breeze.WebApi2;
using LMF.Contracts.Security.Models;
using LMF.DataAccess;
using LMF.DataAccess.Security;
using Newtonsoft.Json.Linq;

namespace LMF.Web.Controllers
{
    [BreezeController]
    [Authorize]
    public class ModelsController : ApiController
    {
        private readonly ModelsContextProvider _contextProvider = new ModelsContextProvider();
        private readonly UserRoleRepository _userRoleRepository;

        public ModelsController()
        {
            _userRoleRepository = new UserRoleRepository();
        }

        // ~/breeze/Models/Metadata 
        [HttpGet]
        public string Metadata()
        {
            return _contextProvider.Metadata();
        }

        // ~/breeze/Models/Users
        [HttpGet]
        public IQueryable<User> Users()
        {
            return _contextProvider.Context.Users;
        }

        // ~/breeze/Models/UserRoles
        [HttpGet]
        public IEnumerable<UserRole> UserRoles()
        {
            var emailAddress = GetEmailAddressForCurrentUser();
            if (emailAddress == null) return new List<UserRole>();
            return _userRoleRepository.Find(emailAddress);
        }

        private string GetEmailAddressForCurrentUser()
        {
            var identity = (ClaimsPrincipal) Thread.CurrentPrincipal;
            var emailClaim = identity.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Email);
            if (emailClaim == null) return null;
            
            return emailClaim.Value;
        }

        // ~/breeze/Models/SaveChanges
        [HttpPost]
        public SaveResult SaveChanges(JObject saveBundle)
        {
            return _contextProvider.SaveChanges(saveBundle);
        }
    }
}