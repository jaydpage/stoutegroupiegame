﻿using System.Web.Optimization;

namespace LMF.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                "~/Scripts/q.js",
                "~/Scripts/jquery-2.1.3.js",
                "~/Scripts/jquery.validate.js",
                "~/Scripts/jquery.signalR-2.1.2.js",
                "~/Scripts/angular.js",
                "~/Scripts/angular-animate.js",
                "~/Scripts/angular-route.js",
                "~/Scripts/angular-sanitize.js",
                "~/Scripts/angular-aria.js",
                "~/Scripts/angular-messages.js",
                "~/Scripts/angular-material.js",
                "~/Scripts/toastr.js",
                "~/Scripts/moment.js",
                "~/Scripts/underscore.js",
                "~/Scripts/breeze.debug.js",
                "~/Scripts/breeze.bridge.angular.js",
                "~/Scripts/localforage/localforage.js",
                "~/Scripts/phaser.js",
                "~/Scripts/breeze.directives.js"));

            bundles.Add(new ScriptBundle("~/bundles/game").IncludeDirectory(
                "~/ClientApps/game",
                "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/mainCore").IncludeDirectory(
                "~/ClientApps/main",
                "*.js", false));

            bundles.Add(new ScriptBundle("~/bundles/main").IncludeDirectory(
                "~/ClientApps/main/sections",
                "*.js", true).IncludeDirectory(
                "~/ClientApps/main/layout",
                "*.js", true).IncludeDirectory(
                "~/ClientApps/main/domain",
                "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/mainServices").IncludeDirectory(
                "~/ClientApps/main/services",
                "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/common").IncludeDirectory(
                "~/ClientApps/common",
                "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/login").IncludeDirectory(
                "~/ClientApps/login",
                "*.js", false));

            bundles.Add(new StyleBundle("~/Content/css").IncludeDirectory(
                "~/Content", "*.css", false));
        }
    }
}